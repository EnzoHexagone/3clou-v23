# Guide d'Installation - Hébergement de Drupal dans 1 instance
Ce guide détaille les étapes nécessaires pour déployer et configurer un environnement d'hébergement pour Drupal dans 1 seule instance, en utilisant Tofu comme gestionnaire de configuration et MariaDB avec Nginx comme serveur de base de données et serveur web.

### Installation
1 - Cloner le Projet
```
git clone https://gitlab.com/bugbusters_drupal/cloud_b3.git

cd drupal_oneInstance/
```
2 - Configuration de Tofu

Assurez-vous d'avoir configuré Tofu avec les informations d'accès à votre service cloud. Vous pouvez consulter la documentation officielle de Tofu pour plus de détails.

3 - Lancement de Tofu
```
tofu init
tofu apply
 - yes
```

Ces commandes permette de créer une instance, installer et configurer php, nginx, MariaDB et Drupal.


### Utilisation
Une fois l'installation et la configuration terminées, vous pouvez accéder à votre site Drupal via le navigateur en accédant à l'adresse IP de votre instance cloud ou via votre domaine configuré.
Assurez-vous de personnaliser et de sécuriser davantage votre installation Drupal selon vos besoins spécifiques.
